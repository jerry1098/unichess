package chess;

/*
Name: Jeremias Friedrich
Matrikelnummer: 2168177
Datum: 18.12.2019
 */

import chess.piece.ChessPiece;
import chess.piece.Knight;
import chess.piece.Queen;

import java.util.Arrays;
import java.util.Scanner;

public class Game {

    Board board;
    Scanner scanner;
    Color color;
    String[] selectedFigureStart;
    String[] selectedDestination;
    boolean startingField;
    String errorMessage;

    public boolean pawnPromotion = false;

    public static void main(String[] args) {
      Game game = new Game();
    }

    Game() {
        welcomePlayer();        //welcome player and give him instructions how to play the game

        //initializing global variables
        scanner = new Scanner(System.in);
        selectedFigureStart = new String[2];
        selectedDestination = new String[2];

        //initializing board
        board = new Board();

        if (getPlayerInputForWelcomeMessage()) {
            printInstructions();
        } else {
            mainGameLoop();
        }
    }

    //the main game loop of the game that runs till the game ends
    void mainGameLoop() {
        color = Color.WHITE; //white player starts
        while (board.hasPlayerWon() == null && !board.isDraw(color)) {
            startingField = true;
            printGameScreen("");
            getPlayerInputWhilePlaying();

            startingField = false;
            printGameScreen("");
            getPlayerInputWhilePlaying();

            int[] selectedFigureStartInt = convertPlayerInputToArrayPos(selectedFigureStart);
            int[] selectedDestinationInt = convertPlayerInputToArrayPos(selectedDestination);
            board.move(color, selectedFigureStartInt[0], selectedFigureStartInt[1], selectedDestinationInt[0], selectedDestinationInt[1], this);

            if(pawnPromotion) {
                getPlayerInputForPawnPromotion();
                pawnPromotion = false;
            }

            if (board.isPlayerCheck(color)) {
                board.playerLost = color;
            }


            if(color == Color.WHITE) {
                color = Color.BLACK;
            } else {
                color = Color.WHITE;
            }
            selectedFigureStart[0] = null;
            selectedFigureStart[1] = null;
            selectedDestination[0] = null;
            selectedDestination[1] = null;
        }

        if (board.hasPlayerWon() == null) {
            printDrawScreen();
        } else {
            printEndScreen(board.hasPlayerWon());
        }
    }

    void printDrawScreen() {
        clearConsoleWindow();
        StringBuilder output = new StringBuilder();
        output.append(convertStringToLength("DRAW", 80));
        output.append(convertStringToLength("good luck next time", 80));
        output.append("\n".repeat(10));
        System.out.println(output.toString());
    }

    //recursive function to get player input until a correct input is given by the user
    void getPlayerInputWhilePlaying() {
        String input = scanner.nextLine();
        input = input.toLowerCase();
        errorMessage = "Wrong input, please input field in this format: [row][column] (e.g. a8)";
        if(!parseInputToPlayerPosition(input)) {
            printGameScreen(errorMessage);
            getPlayerInputWhilePlaying();
        }
    }

    void getPlayerInputForPawnPromotion() {
        String output = "Please input\n" +
                "[1] for a Queen\n" +
                "or\n" +
                "[2] for a Knight";
        for (String line : output.split("\n")) {
            System.out.println(convertStringToLength(line, 20));
        }
        String input = scanner.nextLine();
        input = input.toLowerCase();
        errorMessage = "Please input [1] or [2], depending on your choice";
        if(!parseInputForPawnPromotion(input)) {
            System.out.println(errorMessage);
            getPlayerInputForPawnPromotion();
        }
    }

    boolean parseInputForPawnPromotion(String input) {
        int inputInt;
        try {
            inputInt = Integer.parseInt(input);
        } catch (Exception e) {
            return false;
        }
        switch (inputInt) {
            case 1:
                board.replacePawn(convertPlayerInputToArrayPos(selectedDestination), new Queen(color));
                return true;
            case 2:
                board.replacePawn(convertPlayerInputToArrayPos(selectedDestination), new Knight(color));
                return true;
            default:
                return false;
        }
    }


    //printing "endScreen" depending on the given color
    void printEndScreen(Color color1) {
        clearConsoleWindow();
        Color colorLost = Color.BLACK;
        if (color1 == Color.BLACK) colorLost = Color.WHITE;
        StringBuilder output = new StringBuilder();
        for (String line : Constants.PLAYER_WON.split("\n")) {
            line = convertStringToLength(line.replace("{player_won}", color1.toString()), 80);
            line = convertStringToLength(line.replace("{player_lost}", colorLost.toString()), 80);
            output.append(line);
            output.append("\n");
        }
        output.append("\n".repeat(10));
        System.out.println(output.toString());
    }

    //function to parse the given user input to a position on the board and set this position to selectedFigureStart or selectedDestination depending on the state of the startingField boolean
    boolean parseInputToPlayerPosition(String input) {
        if (input.length() != 2) return false;
        String[] tempArray = new String[2];
        char[] inputArray = input.toCharArray();
        switch (inputArray[0]) {
            case 'a':
                tempArray[0] = "a";
                break;
            case 'b':
                tempArray[0] = "b";
                break;
            case 'c':
                tempArray[0] = "c";
                break;
            case 'd':
                tempArray[0] = "d";
                break;
            case 'e':
                tempArray[0] = "e";
                break;
            case 'f':
                tempArray[0] = "f";
                break;
            case 'g':
                tempArray[0] = "g";
                break;
            case 'h':
                tempArray[0] = "h";
                break;
            default:
                return false;
        }
        switch (inputArray[1]) {
            case '1':
                tempArray[1] = "1";
                break;
            case '2':
                tempArray[1] = "2";
                break;
            case '3':
                tempArray[1] = "3";
                break;
            case '4':
                tempArray[1] = "4";
                break;
            case '5':
                tempArray[1] = "5";
                break;
            case '6':
                tempArray[1] = "6";
                break;
            case '7':
                tempArray[1] = "7";
                break;
            case '8':
                tempArray[1] = "8";
                break;
            default:
                return false;
        }
        int[] tempArrayInt = convertPlayerInputToArrayPos(tempArray);
        if (startingField) {
            ChessPiece chessPieceAtStartingPos = board.getBoard()[tempArrayInt[0]][tempArrayInt[1]];
            if (chessPieceAtStartingPos != null) {
                if (chessPieceAtStartingPos.getColor() == color) {
                    if (canMoveChessPiece(chessPieceAtStartingPos)) {
                        selectedFigureStart = tempArray;
                        return true;
                    } else {
                        errorMessage = "You can't move this figure anywhere, please select another one";
                        return false;
                    }
                } else {
                    errorMessage = "You can only move your own figures";
                    return false;
                }
            } else {
                errorMessage = "This field is empty, please select a field with one off your figures";
                return false;
            }
        } else {
            int[] selectedFigureStartInt = convertPlayerInputToArrayPos(selectedFigureStart);
            if (board.getBoard()[selectedFigureStartInt[0]][selectedFigureStartInt[1]].getPossibleDestinations(board)[tempArrayInt[0]][tempArrayInt[1]]) {
                selectedDestination = tempArray;
                return true;
            } else {
                errorMessage = "You can't move your figure to " + Arrays.toString(tempArray);
                return false;
            }
        }
    }

    boolean canMoveChessPiece(ChessPiece chessPiece) {
        boolean[][] possibleDestinations = chessPiece.getPossibleDestinations(board);
        for (boolean[] row : possibleDestinations) {
            for (boolean singleFiled : row) {
                if(singleFiled) return true;
            }
        }
        return false;
    }

    //converting player input like a8 to the corresponding array position like [0,7]
    int[] convertPlayerInputToArrayPos(String[] playerInput) {
        int[] arrayPos = new int[2];
        arrayPos[1] = playerInput[0].toCharArray()[0] - 97;
        arrayPos[0] = Integer.parseInt(playerInput[1]) - 1;
        return arrayPos;
    }

    //printing the current state of the game to the console depending of the value of the class variables and the given error message
    void printGameScreen(String errorMessage) {
        StringBuilder output = new StringBuilder();
        boolean whiteCheck = board.isPlayerCheck(Color.WHITE);
        boolean blackCheck = board.isPlayerCheck(Color.BLACK);
        if (color == Color.WHITE) output.append("it's \033[0;30;47mwhite\033[0m player's turn"); //24
        if (color == Color.BLACK) output.append("it's \033[0;37;40mblack\033[0m player's turn");
        if (whiteCheck && !blackCheck) output.append(" ".repeat(28)).append("\033[1;37;41mATTENTION\033[0m \033[0;30;47mwhite\033[0m player check"); //28
        if (blackCheck && !whiteCheck) output.append(" ".repeat(28)).append("\033[1;37;41mATTENTION\033[0m \033[0;37;40mblack\033[0m player check");
        if (blackCheck && whiteCheck) output.append(" ".repeat(19)).append("\033[1;37;41mATTENTION\033[0m \033[0;37;40mblack\033[0m and \033[0;30;47mwhite\033[0m player check"); // 39
        output.append("\n".repeat(2));
        if(selectedFigureStart[0] != null) {
            int[] selectedFigureStartInt = convertPlayerInputToArrayPos(selectedFigureStart);
            ChessPiece selectedChessPiece = board.getBoard()[selectedFigureStartInt[0]][selectedFigureStartInt[1]];
            boolean[][] selectedPlayerPos = new boolean[8][8];
            selectedPlayerPos[selectedFigureStartInt[0]][selectedFigureStartInt[1]] = true;
            output.append(board.toString(selectedChessPiece.getPossibleDestinations(board), selectedPlayerPos));
        } else {
            output.append(board.toString());
        }
        if (selectedFigureStart[0] != null) {
            output.append("Figure at: ").append(selectedFigureStart[0]).append(":").append(selectedFigureStart[1]).append(" is selected");
        }
        output.append("\n");
        if (errorMessage.length() > 0) {
            output.append("\n").append(errorMessage).append("\n\n");
        } else {
            output.append("\n".repeat(3));
        }
        if (startingField) output.append("Please input starting field: ");
        if (!startingField) output.append("Please input destination field: ");
        System.out.println(output.toString());
    }

    void clearConsoleWindow() {
        System.out.println("\n".repeat(35));
    }


    //method to handle play input for the welcome message
    boolean getPlayerInputForWelcomeMessage(){
        String input = scanner.nextLine();
        if(input != null && input.length() > 0) {
            try {
                int inputInt = Integer.parseInt(input);
                switch (inputInt) {
                    case 1:
                        return true;
                    case 2:
                        return false;
                    default:
                        System.out.println("This input is not allowed, please press 1 or 2");
                        getPlayerInputForWelcomeMessage();
                }
            } catch (Exception e) {
                System.out.println("This input is not allowed, please press 1 or 2");
                getPlayerInputForWelcomeMessage();
            }
        } else {
            System.out.println("Please press 1 or 2 before pressing enter");
            getPlayerInputForWelcomeMessage();
        }
        return false;
    }

    // printing instructions how to play the game and waiting for player to press enter
    void printInstructions() {
        clearConsoleWindow();
        for (String line : Constants.INSTRUCTIONS.split("\n")) {
            System.out.println(convertStringToLength(line, 80));
        }
        System.out.println("\n".repeat(4));
        scanner.nextLine();
        mainGameLoop();
    }

    //welcome player and give him first instructions to resize the window to the correct size
    void welcomePlayer() {
        int indent = 15;
        StringBuilder output = new StringBuilder();
        output.append("\u250F").append("\u2501 ".repeat(indent + 9 + indent)).append("\u2513");
        for (int i = 0; i < 4; i++) {
            if ((i & 1) != 0) {
                output.append("\u2503").append(" ".repeat(78)).append("\u2503").append("\n");
            } else {
                output.append(" ".repeat(80)).append("\n");
            }
        }
        output.append(" ".repeat(indent - 1));
        output.append("\u250F").append("\u2501".repeat(50)).append("\u2513");
        output.append("\n");
        //adding information box in the center of the screen
        for (int i = 0; i < 12; i++) {
            if ((i & 1) == 0) {
                output.append("\u2503");
                output.append(" ".repeat(indent - 2)).append("\u2503");
            } else {
                output.append(" ".repeat(indent - 1)).append("\u2503");
            }
            switch (i) {
                case 0:
                    output.append(convertStringToLength(Constants.WELCOME_MESSAGE_LINE_1, 50));
                    break;
                case 2:
                    output.append(convertStringToLength(Constants.WELCOME_MESSAGE_LINE_2, 50));
                    break;
                case 5:
                    output.append(convertStringToLength(Constants.WELCOME_MESSAGE_LINE_3, 50));
                    break;
                case 6:
                    output.append(convertStringToLength(Constants.WELCOME_MESSAGE_LINE_4, 50));
                    break;
                case 7:
                    output.append(convertStringToLength(Constants.WELCOME_MESSAGE_LINE_5, 50));
                    break;
                case 9:
                    output.append(convertStringToLength(Constants.WELCOME_MESSAGE_LINE_6, 50));
                    break;
                case 10:
                    output.append(convertStringToLength(Constants.WELCOME_MESSAGE_LINE_7, 50));
                    break;
                default:
                    output.append(" ".repeat(50));
            }
            output.append("\u2503");
            if ((i & 1) == 0) {
                output.append(" ".repeat(indent - 2)).append("\u2503").append("\n");
            } else {
                output.append(" ".repeat(indent - 1)).append("\n");
            }
        }
        output.append("\u2503").append(" ".repeat(13)).append("\u2517").append("\u2501".repeat(50)).append("\u251B").append(" ".repeat(13)).append("\u2503").append("\n");
        for (int i = 0; i < 4; i++) {
            if ((i & 1) != 0) {
                output.append("\u2503").append(" ".repeat(78)).append("\u2503").append("\n");
            } else {
                output.append(" ".repeat(80)).append("\n");
            }
        }
        output.append("\u2517").append("\u2501 ".repeat(indent + 9 + indent)).append("\u251B").append("\n");
        output.append(Constants.WELCOME_MESSAGE_LINE_8);
        System.out.println(output.toString());
    }


    //make string a certain length for nicer output
    String convertStringToLength(String input, int length) {
        if (input.length() >= length) return input;
        StringBuilder output = new StringBuilder();
        int totalSpacesNeeded = length - input.length();
            if ((totalSpacesNeeded & 1) == 0) {
                output.append(" ".repeat(totalSpacesNeeded/2));
                output.append(input);
                output.append(" ".repeat(totalSpacesNeeded/2));
            } else {
                output.append(" ".repeat(totalSpacesNeeded/2));
                output.append(input);
                output.append(" ".repeat((totalSpacesNeeded/2) + 1));
            }

        return output.toString();
    }

}
