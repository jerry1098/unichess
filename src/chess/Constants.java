package chess;

public class Constants {

    public static String WELCOME_MESSAGE_LINE_1 = "";
    public static String WELCOME_MESSAGE_LINE_2 = "Welcome to Chess";
    public static String WELCOME_MESSAGE_LINE_3 = "Do you need instructions?";
    public static String WELCOME_MESSAGE_LINE_4 = "[ 1 ] Yes";
    public static String WELCOME_MESSAGE_LINE_5 = "[ 2 ] No ";
    public static String WELCOME_MESSAGE_LINE_6 = "Please resize window that the frame";
    public static String WELCOME_MESSAGE_LINE_7 = "just fits the terminal size";
    public static String WELCOME_MESSAGE_LINE_8 = "If the content above seems broken just resize window";

    public static String INSTRUCTIONS = "This chess game is using the standard chess rules\n" +
            "you can find them here: https://en.wikipedia.org/wiki/Rules_of_chess\n\n" +
            "Like always white player begins\n" +
            "you can move by first typing in the starting position\n" +
            "and then the destination\n" +
            "the format needs to be the column followed by the row\n" +
            "like a5 or c4\n" +
            "if the figure you're trying to select is unable to move anywhere\n" +
            "you can just select another one\n" +
            "the game ends if you beat your opponents king or\n" +
            "you're still check after you moved\n" +
            "all special rules are included\n" +
            "if you're done reading just press enter";

    public static String PLAYER_WON = "Congrats\n" +
            "{player_won} has won this round\n" +
            "good luck next time {player_lost}";

}
