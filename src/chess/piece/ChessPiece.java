package chess.piece;

import chess.Board;
import chess.Color;

import java.util.Arrays;

public abstract class ChessPiece {
    ChessPieceType type;
    Color color;
    boolean moved = false;

    public ChessPiece(ChessPieceType type, Color color) {
        this.type = type;
        this.color = color;
    }

    public ChessPieceType getType() {
        return type;
    }

    public Color getColor() {
        return color;
    }

    public abstract String toString();

    @Override
    public boolean equals(Object object) {
        return this == object;
    }

    public boolean[][] getPossibleDestinations(Board board) {
        boolean[][] canMoveBoard = new boolean[8][8];
        int[] startingPos = board.getPosition(this);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                canMoveBoard[i][j] = this.canMove(board,startingPos, new int[]{i, j});
            }
        }
        return canMoveBoard;
    }

    public abstract boolean canMove(Board board, int[] start, int[] dest);

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

    public static boolean isChessPieceBetween(Board board, int[] start, int[] dest) {
        int distX = (start[0] - dest[0]) * -1;
        int distY = (start[1] - dest[1]) * -1;

        int incrementX = (distX >= 0) ? 1 : -1;
        int incrementY = (distY >= 0) ? 1 : -1;

        if (distX == 0) incrementX = 0;
        if (distY == 0) incrementY = 0;

        int currentX = start[0], currentY = start[1];

        currentX += incrementX;
        currentY += incrementY;

        while (!(currentX == dest[0] && currentY == dest[1])) {
            if (board.getBoard()[currentX][currentY] != null) return true;

            currentX += incrementX;
            currentY += incrementY;
        }

        return false;
    }
}
