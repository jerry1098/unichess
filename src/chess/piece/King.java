package chess.piece;

import chess.Board;
import chess.Color;

public class King extends ChessPiece {

    public King(Color color) {
        super(ChessPieceType.KING, color);
    }


    //returning unicode representation of king
    @Override
    public String toString() {
        if (color == Color.BLACK) return "\u265A";
        if (color == Color.WHITE) return "\u2654";
        return null;
    }

    @Override
    public boolean canMove(Board board, int[] start, int[] dest) {
        if (board.isKingCheckAfterMove(start, dest, color)) return false;
        if ((Math.abs(start[0] - dest[0]) == 1 && start[1] - dest[1] == 0) || (start[0] - dest[0] == 0 && Math.abs(start[1] - dest[1]) == 1) || Math.abs(start[0] - dest[0]) == Math.abs(start[1] - dest[1]) && Math.abs(start[0] - dest[0]) == 1) {
            return board.getBoard()[dest[0]][dest[1]] == null && !board.isFieldAttack(dest, color) || board.getBoard()[dest[0]][dest[1]] != null && board.getBoard()[dest[0]][dest[1]].color != color && !board.isFieldAttack(dest, color);
        } else if (!moved && !board.isFieldAttack(start, color)) {
            if (Math.abs(start[1] - dest[1]) == 2 && start[0] - dest[0] == 0) {
                if (color == Color.WHITE) {
                    if (start[1] - dest[1] == 2 && board.getBoard()[0][0].getClass() == Rook.class) {
                        Rook rook = (Rook) board.getBoard()[0][0];
                        if (!rook.moved) {
                            return !isChessPieceBetween(board, start, dest) && board.getBoard()[dest[0]][dest[1]] == null && !board.isFieldAttack(dest, color) && board.getBoard()[0][1] == null;
                        }
                    } else if (start[1] - dest[1] == -2 && board.getBoard()[0][7].getClass() == Rook.class){
                        Rook rook = (Rook) board.getBoard()[0][7];
                        if (!rook.moved) {
                            return !isChessPieceBetween(board, start, dest) && board.getBoard()[dest[0]][dest[1]] == null && !board.isFieldAttack(dest, color);
                        }
                    }
                } else {
                    if (start[1] - dest[1] == 2 && board.getBoard()[7][0].getClass() == Rook.class) {
                        Rook rook = (Rook) board.getBoard()[7][0];
                        if (!rook.moved) {
                            return !isChessPieceBetween(board, start, dest) && board.getBoard()[dest[0]][dest[1]] == null && !board.isFieldAttack(dest, color) && board.getBoard()[7][1] == null;
                        }
                    } else if (start[1] - dest[1] == -2 && board.getBoard()[7][7].getClass() == Rook.class){
                        Rook rook = (Rook) board.getBoard()[7][7];
                        if (!rook.moved) {
                            return !isChessPieceBetween(board, start, dest) && board.getBoard()[dest[0]][dest[1]] == null && !board.isFieldAttack(dest, color);
                        }
                    }
                }
            }
        }
        return false;
    }

}
