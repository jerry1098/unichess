package chess.piece;

import chess.Board;
import chess.Color;

public class Rook extends ChessPiece {

    public Rook(Color color) {
        super(ChessPieceType.ROOK, color);
    }


    //returning unicode representation of rook
    @Override
    public String toString() {
        if (color == Color.BLACK) return "\u265C";
        if (color == Color.WHITE) return "\u2656";
        return null;
    }

    @Override
    public boolean canMove(Board board, int[] start, int[] dest) {
        if (board.isKingCheckAfterMove(start, dest, color)) return false;
        if ((start[0] == dest[0] || start[1] == dest[1])) {
            if (!isChessPieceBetween(board, start, dest)) {
                return board.getBoard()[dest[0]][dest[1]] == null || board.getBoard()[dest[0]][dest[1]].color != color;
            } else {
                return false;
            }
        }
        return false;
    }

}
