package chess.piece;

import chess.Board;
import chess.Color;

public class Bishop extends ChessPiece {

    public Bishop(Color color) {
        super(ChessPieceType.BISHOP, color);
    }


    //returning unicode representation of bishop
    @Override
    public String toString() {
        if (color == Color.BLACK) return "\u265D";
        if (color == Color.WHITE) return "\u2657";
        return null;
    }

    @Override
    public boolean canMove(Board board, int[] start, int[] dest) {
        if (board.isKingCheckAfterMove(start, dest, color)) return false;
        if (Math.abs(start[0] - dest[0]) == Math.abs(start[1] - dest[1])) {
            if (!isChessPieceBetween(board, start, dest)) {
                return board.getBoard()[dest[0]][dest[1]] == null || board.getBoard()[dest[0]][dest[1]].color != color;
            } else {
                return false;
            }
        }
        return false;
    }

}
