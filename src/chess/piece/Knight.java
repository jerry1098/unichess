package chess.piece;

import chess.Board;
import chess.Color;

public class Knight extends ChessPiece {

    public Knight(Color color) {
        super(ChessPieceType.KNIGHT, color);
    }


    //returning unicode representation of knight
    @Override
    public String toString() {
        if (color == Color.BLACK) return "\u265E";
        if (color == Color.WHITE) return "\u2658";
        return null;
    }

    @Override
    public boolean canMove(Board board, int[] start, int[] dest) {
        if (board.isKingCheckAfterMove(start, dest, color)) return false;
        if ((Math.abs(start[0] - dest[0]) == 2 && Math.abs(start[1] - dest[1]) == 1) || (Math.abs(start[0] - dest[0]) == 1 && Math.abs(start[1] - dest[1]) == 2)) {
            return board.getBoard()[dest[0]][dest[1]] == null || board.getBoard()[dest[0]][dest[1]].getColor() != color;
        }
        return false;
    }

}
