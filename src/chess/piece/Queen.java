package chess.piece;

import chess.Board;
import chess.Color;

import java.util.Arrays;

public class Queen extends ChessPiece {

    public Queen( Color color) {
        super(ChessPieceType.QUEEN, color);
    }


    //returning unicode representation of queen
    @Override
    public String toString() {
        if (color == Color.BLACK) return "\u265B";
        if (color == Color.WHITE) return "\u2655";
        return null;
    }

    @Override
    public boolean canMove(Board board, int[] start, int[] dest) {
        if (board.isKingCheckAfterMove(start, dest, color)) return false;
        if ((start[0] == dest[0] || start[1] == dest[1]) || Math.abs(start[0] - dest[0]) == Math.abs(start[1] - dest[1])) {
            if (!isChessPieceBetween(board, start, dest)) {
                return board.getBoard()[dest[0]][dest[1]] == null || board.getBoard()[dest[0]][dest[1]].color != color;
            } else {
                return false;
            }
        }
        return false;
    }

}
