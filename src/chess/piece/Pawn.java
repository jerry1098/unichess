package chess.piece;

import chess.Board;
import chess.Color;

public class Pawn extends ChessPiece{

    public boolean enPassantBeatable = false;

    public Pawn( Color color) {
        super(ChessPieceType.PAWN, color);
    }


    //returning unicode representation of pawn
    @Override
    public String toString() {
        if (color == Color.BLACK) return "\u265F";
        if (color == Color.WHITE) return "\u2659";
        return null;
    }

    public boolean movedTwoFields(int[] start, int[] dest) {
        if (color == Color.WHITE) {
            return dest[0] - start[0] == 2 && dest[1] - start[1] == 0;
        } else {
            return start[0] - dest[0] == 2 && start[1] - dest[1] == 0;
        }
    }

    @Override
    public boolean canMove(Board board, int[] start, int[] dest) {
        if (board.isKingCheckAfterMove(start, dest, color)) return false;
        if (color == Color.WHITE) {
            if (dest[0] - start[0] == 1 && dest[1] - start[1] == 0) {
                return board.getBoard()[dest[0]][dest[1]] == null;
            } else if (dest[0] - start[0] == 2 && !moved && dest[1] - start[1] == 0) {
                if (!isChessPieceBetween(board, start, dest)) {
                    return board.getBoard()[dest[0]][dest[1]] == null;
                } else {
                    return false;
                }
            } else if (dest[0] - start[0] == 1 && Math.abs(dest[1] - start[1]) == 1) {
                if (board.getBoard()[dest[0]][dest[1]] != null) {
                    return board.getBoard()[dest[0]][dest[1]].color != color;
                } else {
                    if (board.getBoard()[dest[0] - 1][dest[1]] != null && board.getBoard()[dest[0] - 1][dest[1]].getClass() == Pawn.class) {
                        Pawn pawn = (Pawn) board.getBoard()[dest[0] - 1][dest[1]];
                        return pawn.enPassantBeatable;
                    }
                }
                return false;
            }
        } else {
            if (start[0] - dest[0] == 1 && start[1] - dest[1] == 0) {
                return board.getBoard()[dest[0]][dest[1]] == null;
            } else if (start[0] - dest[0] == 2 && !moved && start[1] - dest[1] == 0) {
                if (!isChessPieceBetween(board, start, dest)) {
                    return board.getBoard()[dest[0]][dest[1]] == null;
                } else {
                    return false;
                }
            } else if (start[0] - dest[0] == 1 && Math.abs(start[1] - dest[1]) == 1) {
                if (board.getBoard()[dest[0]][dest[1]] != null) {
                    return board.getBoard()[dest[0]][dest[1]].color != color;
                } else {
                    if (board.getBoard()[dest[0] + 1][dest[1]] != null && board.getBoard()[dest[0] + 1][dest[1]].getClass() == Pawn.class) {
                        Pawn pawn = (Pawn) board.getBoard()[dest[0] + 1][dest[1]];
                        return pawn.enPassantBeatable;
                    }
                }
                return false;
            }
        }
        return false;
    }

}
