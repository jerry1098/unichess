package chess;

import chess.piece.*;

import java.util.Arrays;

public class Board {

    //representation of board
    private ChessPiece[][] board;
    Color playerLost = null;

    //boolean to prevent recursive call off this function ending in an infinite loop causing a stackoverflow
    static boolean isKingCheckAfterMoveAlreadyCalled = false;


    //initializing board with default line-up
    public Board() {
        board = new ChessPiece[8][8];

        //initializing white team
        board[0][0] = new Rook(Color.WHITE);
        board[0][1] = new Knight(Color.WHITE);
        board[0][2] = new Bishop(Color.WHITE);
        board[0][3] = new Queen(Color.WHITE);
        board[0][4] = new King(Color.WHITE);
        board[0][5] = new Bishop(Color.WHITE);
        board[0][6] = new Knight(Color.WHITE);
        board[0][7] = new Rook(Color.WHITE);

        for (int i = 0; i < 8; i++) {
            board[1][i] = new Pawn(Color.WHITE);
        }


        //initializing black team
        board[7][0] = new Rook(Color.BLACK);
        board[7][1] = new Knight(Color.BLACK);
        board[7][2] = new Bishop(Color.BLACK);
        board[7][3] = new Queen(Color.BLACK);
        board[7][4] = new King(Color.BLACK);
        board[7][5] = new Bishop(Color.BLACK);
        board[7][6] = new Knight(Color.BLACK);
        board[7][7] = new Rook(Color.BLACK);

        for (int i = 0; i < 8; i++) {
            board[6][i] = new Pawn(Color.BLACK);
        }
    }


    //initializing with custom line-up
    public Board(ChessPiece[][] board) {
        this.board = board;
    }


    //returning current board line-up
    public ChessPiece[][] getBoard() {
        return board;
    }


    //returning position off a given chessPiece
    public int[] getPosition(ChessPiece piece) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board[i][j] == piece) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[]{};
    }

    public void move(Color player, int rowCur, int colCur, int rowDes, int colDes) {
        move(player, rowCur, colCur, rowDes, colDes, null);
    }

    //move figure form current position to destination
    public void move(Color player, int rowCur, int colCur, int rowDes, int colDes, Game game) {

        //resetting enPassantBeatable to false
        for (ChessPiece[] chessPiecesRow : board) {
            for (ChessPiece chessPiece : chessPiecesRow) {
                if (chessPiece != null && chessPiece.getClass() == Pawn.class && chessPiece.getColor() == player) {
                    Pawn pawn = (Pawn) chessPiece;
                    pawn.enPassantBeatable = false;
                }
            }
        }

        //castling
        if (board[rowCur][colCur].getClass() == King.class) {
            King king = (King) board[rowCur][colCur];
            if (Math.abs(colCur - colDes) == 2 && rowCur - rowDes == 0) {
                if (colDes == 6) {
                    board[rowDes][5] = board[rowDes][7];
                    board[rowDes][7] = null;
                } else if (colDes == 2) {
                    board[rowDes][3] = board[rowDes][0];
                    board[rowDes][0] = null;
                }
            }
        }

        //Remove enPassant beaten pawn from board
        if (board[rowCur][colCur].getClass() == Pawn.class) {
            Pawn pawn = (Pawn) board[rowCur][colCur];
            if (pawn.getColor() == Color.WHITE) {
                if (rowDes - rowCur == 1 && Math.abs(colDes - colCur) == 1) {
                    if (board[rowDes - 1][colDes] != null && board[rowDes - 1][colDes].getClass() == Pawn.class) {
                        Pawn secondPawn = (Pawn) board[rowDes - 1][colDes];
                        if (secondPawn.enPassantBeatable) {
                            board[rowDes - 1][colDes] = null;
                        }
                    }
                }
            } else {
                if (rowCur - rowDes == 1 && Math.abs(colDes - colCur) == 1) {
                    if (board[rowDes + 1][colDes] != null && board[rowDes + 1][colDes].getClass() == Pawn.class) {
                        Pawn secondPawn = (Pawn) board[rowDes + 1][colDes];
                        if (secondPawn.enPassantBeatable) {
                            board[rowDes + 1][colDes] = null;
                        }
                    }
                }
            }
        }

        moveWithoutChecking(rowCur, colCur, rowDes, colDes);
        board[rowDes][colDes].setMoved(true);

        //set enPassantBeatable to true if a pawn moved two fields
        if (board[rowDes][colDes].getClass() == Pawn.class) {
            Pawn pawn = (Pawn) board[rowDes][colDes];
            if (pawn.movedTwoFields(new int[]{rowCur, colCur}, new int[]{rowDes, colDes})) {
                pawn.enPassantBeatable = true;
            }
        }

        //pawnPromotion
        if (board[rowDes][colDes].getClass() == Pawn.class) {
            if (rowDes == 7 || rowDes == 0) {
                if (game != null) {
                    game.pawnPromotion = true;
                }
            }
        }
    }

    public void moveWithoutChecking(int rowCur, int colCur, int rowDes, int colDes) {
        board[rowDes][colDes] = board[rowCur][colCur];
        board[rowCur][colCur] = null;
    }

    public void replacePawn(int[] position, ChessPiece chessPiece) {
        board[position[0]][position[1]] = chessPiece;
    }


    //checking if given field is attacked by any figure
    public boolean isFieldAttack(int[] field, Color color) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                ChessPiece chessPiece = board[i][j];
                if (chessPiece != null && chessPiece.getClass() != King.class && chessPiece.getColor() != color) {
                    if (chessPiece.canMove(this, new int[]{i, j}, field)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isDraw(Color color) {

        //is king check
        for (ChessPiece[] chessPieces : board) {
            for (ChessPiece chessPiece : chessPieces) {
                if (chessPiece != null && chessPiece.getColor() == color && chessPiece.getClass() == King.class) {
                    if (isFieldAttack(getPosition(chessPiece), color)) return false;
                }
            }
        }

        //is there a way to move any figure
        for (ChessPiece[] chessPieces : board) {
            for (ChessPiece chessPiece : chessPieces) {
                if (chessPiece != null && chessPiece.getColor() == color) {
                    for (boolean[] booleans : chessPiece.getPossibleDestinations(this)) {
                        for (boolean canMove : booleans) {
                            if (canMove) return false;
                        }
                    }
                }
            }
        }
        return true;
    }


    public Color hasPlayerWon() {
        if (playerLost != null) {
            return switchColor(playerLost);
        }

        for (ChessPiece[] chessPieces : board) {
            for (ChessPiece chessPiece : chessPieces) {
                if (chessPiece != null && chessPiece.getClass() == King.class) {
                    King king = (King) chessPiece;
                    if (isKingCheckmate(king)) {
                        return switchColor(king.getColor());
                    }
                }
            }
        }
        return null;
    }

    private Board createTestBoard() {
        ChessPiece[][] newBoard = new ChessPiece[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                newBoard[i][j] = board[i][j];
            }
        }
        return new Board(newBoard);
    }

    public boolean isKingCheckAfterMove(int[] posCur, int[] posDes, Color color) {
        if (isKingCheckAfterMoveAlreadyCalled) return false;
        isKingCheckAfterMoveAlreadyCalled = true;

        Board testBoard = createTestBoard();

        int[] kingPos = new int[2];

        testBoard.moveWithoutChecking(posCur[0], posCur[1], posDes[0], posDes[1]);

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (testBoard.getBoard()[i][j] != null && testBoard.getBoard()[i][j].getClass() == King.class && testBoard.getBoard()[i][j].getColor() == color) {
                    kingPos[0] = i;
                    kingPos[1] = j;
                }
            }
        }

        if (testBoard.isFieldAttack(kingPos, color)) {
            isKingCheckAfterMoveAlreadyCalled = false;
            return true;
        } else {
            isKingCheckAfterMoveAlreadyCalled = false;
            return false;
        }


    }

    private static Color switchColor(Color color) {
        if (color == Color.WHITE) return Color.BLACK;
        return Color.WHITE;
    }

    private boolean isKingCheckmate(King king) {
        if (!isFieldAttack(getPosition(king), king.getColor())) return false;
        for (int rowCur = 0; rowCur < 8; rowCur++) {
            for (int colCur = 0; colCur < 8; colCur++) {
                if (board[rowCur][colCur] != null && board[rowCur][colCur].getColor() == king.getColor()) { //for all figures of the same color
                    ChessPiece chessPiece = board[rowCur][colCur];
                    boolean[][] possDest = chessPiece.getPossibleDestinations(this);
                    for (int rowDes = 0; rowDes < 8; rowDes++) {
                        for (int colDes = 0; colDes < 8; colDes++) {
                            if (possDest[rowDes][colDes]) { //if dest is a possible dest of chessPiece
                                Board posSolutionBoard = createTestBoard();
                                posSolutionBoard.moveWithoutChecking(rowCur, colCur, rowDes, colDes); //move chessPiece to possible position on test board
                                for (ChessPiece[] chessPieces : posSolutionBoard.getBoard()) {
                                    for (ChessPiece chessPiece1 : chessPieces) {
                                        if (chessPiece1 != null && chessPiece1.getClass() == King.class && chessPiece1.getColor() == king.getColor()) {
                                            if (!posSolutionBoard.isFieldAttack(posSolutionBoard.getPosition(chessPiece1), king.getColor()))
                                                return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }


    public boolean isPlayerCheck(Color player) {
        for (ChessPiece[] chessPieces : board) {
            for (ChessPiece chessPiece : chessPieces) {
                if (chessPiece != null && chessPiece.getClass() == King.class && chessPiece.getColor() == player) {
                    if (isFieldAttack(getPosition(chessPiece), player)) return true;
                }
            }
        }
        return false;
    }


    @Override
    public String toString() {
        return toString(21);
    }

    public String toString(int indent) {
        boolean[][] emptyBoolArray = new boolean[8][8];
        return toString(indent, emptyBoolArray, emptyBoolArray);
    }

    public String toString(boolean[][] coloredRed, boolean[][] coloredBlue) {
        return toString(21, coloredRed, coloredBlue);
    }

    //returning string-representation of current board line-up
    public String toString(int indent, boolean[][] coloredRed, boolean[][] coloredBlue) {
        //stringBuilder to build total output string
        StringBuilder output = new StringBuilder();
        //char to iterate over alphabet
        char iterChar = 'a';

        //printing first line
        output.append(" ".repeat(4 + indent));
        for (int i = 0; i < 8; i++) {
            output.append(String.format("  %s ", iterChar));
            iterChar++;
        }
        output.append("\n");

        //printing leftover information
        int rowNumber = 8;  //keeps track of current row
        int colNumber = 0;  //keeps track of current column
        for (int i = 0; i < 17; i++) {
            if ((i & 1) == 0) {     //set even rows to a separator row
                output.append(" ".repeat(4 + indent));
                output.append("-".repeat(33));
            } else {
                output.append(" ".repeat(indent));
                output.append(String.format("  %d ", rowNumber)); //start odd rows with the rowNumber
                //print the rest of the line
                boolean placeChessFigure = false;   //decides if a chess figure or separator should be appended to output
                for (int j = 0; j < 34; j++) {
                    if ((j & 1) == 0) {             //fill every even character place with a char
                        if (placeChessFigure) {
                            if (coloredRed[rowNumber - 1][colNumber]) output.append("\033[0;31m");
                            if (coloredBlue[rowNumber - 1][colNumber]) output.append("\033[0;34m");
                            output.append(board[rowNumber - 1][colNumber] != null ? board[rowNumber - 1][colNumber].toString() : "\u26F6");    //appending depending on the state of the board at a certain position
                            output.append("\033[0m");
                            placeChessFigure = false;
                            colNumber++;
                        } else {
                            output.append("|");
                            placeChessFigure = true;
                        }
                    } else {
                        output.append(" ");         //fill every odd character place with a space
                    }
                }
                rowNumber--;        //moving one row down
            }
            colNumber = 0;          //resetting column number
            output.append("\n");    //appending a new line at the end of current line
        }
        return output.toString();
    }

}
